<?php
/*
Plugin Name: Socialize Plugin
Plugin URI: 
Description: A required plugin for Socialize theme you purchased from ThemeForest. It includes a number of features that you can still use if you switch to another theme.
Version: 3.1.2
Author: GhostPool
Author URI: http://themeforest.net/user/GhostPool/portfolio?ref=GhostPool
License: You should have purchased a license from ThemeForest.net
Text Domain: socialize-plugin
*/

// Ensure latest version of plugin installed
function ghostpool_socialize_plugin_update() {}
	
if ( ! class_exists( 'GhostPool_Socialize' ) ) {

	class GhostPool_Socialize {

		public function __construct() {

			// Load plugin translations
			add_action( 'plugins_loaded', array( &$this, 'ghostpool_plugin_load_textdomain' ) );

			// Load Redux
			add_action( 'plugins_loaded', array( &$this, 'ghostpool_redux' ) );			
						
			// Add shortcode support to Text widget
			add_filter( 'widget_text', 'do_shortcode' );

			// Add excerpt support to pages
			if ( ! function_exists( 'ghostpool_add_excerpts_to_pages' ) ) {
				function ghostpool_add_excerpts_to_pages() {
					 add_post_type_support( 'page', 'excerpt' );
				}
			}
			add_action( 'init', 'ghostpool_add_excerpts_to_pages' );

			// Add post tags support to pages
			if ( ! function_exists( 'ghostpool_page_tags_support' ) ) {
				function ghostpool_page_tags_support() {
					register_taxonomy_for_object_type( 'post_tag', 'page' );
				}
			}
			add_action( 'init', 'ghostpool_page_tags_support' );

			// Display pages in post tag queries
			if ( ! function_exists( 'ghostpool_page_tags_support_query' ) ) {
				function ghostpool_page_tags_support_query( $wp_query ) {
					if ( $wp_query->get( 'tag' ) ) {
						$wp_query->set( 'post_type', 'any' );
					}	
				}
			}
			add_action( 'pre_get_posts', 'ghostpool_page_tags_support_query' );

			// Add user profile fields
			if ( ! function_exists( 'ghostpool_custom_profile_methods' ) ) {
				function ghostpool_custom_profile_methods( $gp_profile_fields ) {
					$gp_profile_fields['twitter'] = esc_html__( 'Twitter URL', 'socialize-plugin' );
					$gp_profile_fields['facebook'] = esc_html__( 'Facebook URL', 'socialize-plugin' );
					$gp_profile_fields['googleplus'] = esc_html__( 'Google+ URL', 'socialize-plugin' );
					$gp_profile_fields['pinterest'] = esc_html__( 'Pinterest URL', 'socialize-plugin' );
					$gp_profile_fields['youtube'] = esc_html__( 'YouTube URL', 'socialize-plugin' );
					$gp_profile_fields['vimeo'] = esc_html__( 'Vimeo URL', 'socialize-plugin' );
					$gp_profile_fields['flickr'] = esc_html__( 'Flickr URL', 'socialize-plugin' );
					$gp_profile_fields['linkedin'] = esc_html__( 'LinkedIn URL', 'socialize-plugin' );
					$gp_profile_fields['instagram'] = esc_html__( 'Instagram URL', 'socialize-plugin' );
					return $gp_profile_fields;
				}
			}
			add_filter( 'user_contactmethods', 'ghostpool_custom_profile_methods' );

			// Load shortcodes
			if ( ! class_exists( 'GhostPool_Shortcodes' ) ) {
				require_once( dirname( __FILE__ ) . '/shortcodes/theme-shortcodes.php' );
				$GhostPool_Shortcodes = new GhostPool_Shortcodes();
			}

			// Load custom sidebars
			if ( ! class_exists( 'GhostPool_Custom_Sidebars' ) ) {
				require_once( dirname( __FILE__ ) . '/custom-sidebars/custom-sidebars.php' );
			}
			
			// Load portfolio post type
			if ( ! post_type_exists( 'gp_portfolio' ) && ! class_exists( 'GhostPool_Portfolio' ) ) {
				require_once( dirname( __FILE__ ) . '/inc/portfolio-tax.php' );
				$GhostPool_Portfolios = new Ghostpool_Portfolios();
			}
			
			// Load slide post type
			if ( ! post_type_exists( 'gp_slide' ) && ! class_exists( 'GhostPool_Slides' ) ) {
				require_once( dirname( __FILE__ ) . '/inc/slide-tax.php' );
				$GhostPool_Slides = new Ghostpool_Slides();
			}

			// One click demo installer
			require_once( dirname( __FILE__ ) . '/demo-installer/init.php' );
																			
		} 
		
		public static function ghostpool_activate() {} 		
		
		public static function ghostpool_deactivate() {}

		public function ghostpool_plugin_load_textdomain() {
			load_plugin_textdomain( 'socialize-plugin', false, trailingslashit( WP_LANG_DIR ) . 'plugins/' );
			load_plugin_textdomain( 'socialize-plugin', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
		}

		public function ghostpool_redux() {
		
			// Remove Redux ads
			require_once( dirname( __FILE__ ) . '/redux-extensions/extensions/ad_remove/extension_ad_remove.php' );

			// Load Redux metaboxes
			require_once( dirname( __FILE__ ) . '/redux-extensions/config.php' );

			// Load Redux theme options framework
			if ( file_exists( dirname( __FILE__ ) . '/redux-framework/framework.php' ) ) {
				require_once( dirname( __FILE__ ) . '/redux-framework/framework.php' );
			}
			if ( file_exists( dirname( __FILE__ ) . '/inc/theme-config.php' ) ) {
				require_once( dirname( __FILE__ ) . '/inc/theme-config.php' );
				function ghostpool_option( $gp_opt_1, $gp_opt_2 = false, $gp_opt_3 = false ) {
					global $socialize;
					if ( $gp_opt_2 ) {
						if ( isset( $socialize[$gp_opt_1][$gp_opt_2] ) ) {
							return $socialize[$gp_opt_1][$gp_opt_2];
						}
					} else {
						if ( isset( $socialize[$gp_opt_1] ) ) {
							return $socialize[$gp_opt_1];
						}
					}
				}
			}	
		
		}
						
	}
	
}

// User registration emails
$gp_theme_variable = get_option( 'socialize' );
if ( ! function_exists( 'wp_new_user_notification' ) && ! function_exists( 'bp_is_active' ) && ( isset ( $gp_theme_variable['popup_box'] ) && $gp_theme_variable['popup_box'] == 'enabled' ) ) {
	function wp_new_user_notification( $gp_user_id, $gp_deprecated = null, $gp_notify = 'both' ) {

		if ( $gp_deprecated !== null ) {
			_deprecated_argument( __FUNCTION__, '4.3.1' );
		}
	
		global $wpdb;
		$gp_user = get_userdata( $gp_user_id );
		
		$gp_user_login = stripslashes( $gp_user->user_login );
		$gp_user_email = stripslashes( $gp_user->user_email );

		// The blogname option is escaped with esc_html on the way into the database in sanitize_option
		// we want to reverse this for the plain text arena of emails.
		$gp_blogname = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
		
		// Admin email
		$gp_message  = sprintf( esc_html__( 'New user registration on your blog %s:', 'socialize-plugin' ), $gp_blogname ) . "\r\n\r\n";
		$gp_message .= sprintf( esc_html__( 'Username: %s', 'socialize-plugin' ), $gp_user_login ) . "\r\n\r\n";
		$gp_message .= sprintf( esc_html__( 'Email: %s', 'socialize-plugin' ), $gp_user_email ) . "\r\n";
		$gp_message = apply_filters( 'gp_registration_notice_message', $gp_message, $gp_blogname, $gp_user_login, $gp_user_email );
		@wp_mail( get_option( 'admin_email' ), sprintf( apply_filters( 'gp_registration_notice_subject', esc_html__( '[%s] New User Registration', 'socialize-plugin' ), $gp_blogname ), $gp_blogname ), $gp_message );

		if ( 'admin' === $gp_notify || empty( $gp_notify ) ) {
			return;
		}
		
		// User email
		$gp_message  = esc_html__( 'Hi there,', 'socialize-plugin' ) . "\r\n\r\n";
		$gp_message .= sprintf( esc_html__( 'Welcome to %s.', 'socialize-plugin' ), $gp_blogname ) . "\r\n\r\n";
		$gp_message .= sprintf( esc_html__( 'Username: %s', 'socialize-plugin' ), $gp_user_login ) . "\r\n";
		$gp_message .= esc_html__( 'Password: [use the password you entered when signing up]', 'socialize-plugin' ) . "\r\n\r\n";
		$gp_message .= 'Please login at ' . home_url( '/#login' ) . "\r\n\r\n";	
		$gp_message = apply_filters( 'gp_registered_user_message', $gp_message, $gp_blogname, $gp_user_login, $gp_user_email );
		wp_mail( $gp_user_email, sprintf( apply_filters( 'gp_registered_user_subject', esc_html__( '[%s] Your username and password', 'socialize-plugin' ), $gp_blogname ), $gp_blogname ), $gp_message );

	}
}

// Active/deactivate plugin
if ( class_exists( 'GhostPool_Socialize' ) ) {

	register_activation_hook( __FILE__, array( 'GhostPool_Socialize', 'ghostpool_activate' ) );
	register_deactivation_hook( __FILE__, array( 'GhostPool_Socialize', 'ghostpool_deactivate' ) );

	$ghostpool_plugin = new GhostPool_Socialize();

}

?>