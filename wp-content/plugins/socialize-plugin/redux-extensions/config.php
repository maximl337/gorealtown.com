<?php

// INCLUDE THIS BEFORE you load your ReduxFramework object config file.


// You may replace $redux_opt_name with a string if you wish. If you do so, change loader.php
// as well as all the instances below.
$redux_opt_name = "socialize";

if ( !function_exists( "gp_add_metaboxes" ) ):
    function ghostpool_add_metaboxes($metaboxes) {

    $metaboxes = array();
             
                
	/*--------------------------------------------------------------
	Post Options
	--------------------------------------------------------------*/	

	// Audio Post Format Options

    $audio_format_options = array();
    $audio_format_options[] = array(
		'fields' => array(
						        
			array(
				'id'        => 'audio_mp3_url',
				'type'      => 'media',
				'title'     => esc_html__( 'MP3 Audio File', 'socialize-plugin' ),
				'mode'      => false,
				'desc'      => esc_html__( 'Upload a MP3 audio file.', 'socialize-plugin' ),
			),

			array(
				'id'        => 'audio_ogg_url',
				'type'      => 'media',
				'title'     => esc_html__( 'OGG Audio File', 'socialize-plugin' ),
				'mode'      => false,
				'desc'      => esc_html__( 'Upload a OGG audio file.', 'socialize-plugin' ),
			),
					
		),
	);	
    $metaboxes[] = array(
        'id' => 'audio-format-options',
        'title' => esc_html__( 'Audio Options', 'socialize-plugin' ),
        'post_types' => array( 'post' ),
        'post_format' => array( 'audio' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $audio_format_options,
    );
    	
    	
	// Gallery Post Format Options

    $gallery_format_options = array();
    $gallery_format_options[] = array(
        'fields' => array(
						        
			array(
				'id'        => 'gallery_slider',
				'type'      => 'gallery',
				'title'     => esc_html__( 'Gallery Slider', 'socialize-plugin' ),
				 'subtitle'  => esc_html__( 'Create a new gallery by selecting an existing one or uploading new images using the WordPress native uploader.', 'socialize-plugin' ),
				 'desc'  => esc_html__( 'Add a gallery slider.', 'socialize-plugin' ),
			),
 
		),
	);		
    $metaboxes[] = array(
        'id' => 'gallery-format-options',
        'title' => esc_html__( 'Gallery Options', 'socialize-plugin' ),
        'post_types' => array( 'post' ),
        'post_format' => array( 'gallery' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $gallery_format_options,
    );
    
    
    // Link Format Options
    
    $link_format_options = array();
    $link_format_options[] = array(
        'fields' => array(
						        
			array(
				'id'       => 'link',
				'type'     => 'text',
				'title'    => esc_html__( 'Link', 'socialize-plugin' ),
				'desc'     => esc_html__( 'The link which your post goes to.', 'socialize-plugin' ),
				'validate' => 'url',
			),
			
			array( 
				'id' => 'link_target',
				'title' => esc_html__( 'Link Target', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'The target for the link.', 'socialize-plugin' ),
				'options' => array(
					'_blank' => esc_html__( 'New Window', 'socialize-plugin' ),
					'_self' => esc_html__( 'Same Window', 'socialize-plugin' ),
				),
				'default' => '_blank',
			),
					 
		),
	);		
    $metaboxes[] = array(
        'id' => 'link-format-options',
        'title' => esc_html__( 'Link Options', 'socialize-plugin' ),
        'post_types' => array( 'post' ),
        'post_format' => array( 'link' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $link_format_options,
    );
    
    
    // Quote Format Options
    
    $quote_format_options = array();
    $quote_format_options[] = array(
        'fields' => array(
						        
			array(
				'id'       => 'quote_source',
				'type'     => 'text',
				'title'    => esc_html__( 'Quote Source', 'socialize-plugin' ),
				'desc'     => esc_html__( 'The source of the quote.', 'socialize-plugin' ),
			),
					 
		),
	);
    $metaboxes[] = array(
        'id' => 'quote-format-options',
        'title' => esc_html__( 'Quote Options', 'socialize-plugin' ),
        'post_types' => array( 'post' ),
        'post_format' => array( 'quote' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $quote_format_options,
    );
    
            
    // Video Format Options
    
    $video_format_options = array();
    $video_format_options[] = array(
        'fields' => array(
			
			array(
				'id'        => 'video_embed_url',
				'type'      => 'text',
				'title'     => esc_html__( 'Video URL', 'socialize-plugin' ),
				'desc'      => esc_html__( 'Video URL uploaded to one of the major video sites e.g. YouTube, Vimeo, blip.tv, etc.', 'socialize-plugin' ),
				'validate'  => 'url',
				'default' => '',
			),
			        
			array(
				'id'        => 'video_m4v_url',
				'type'      => 'media',
				'title'     => esc_html__( 'M4V Video', 'socialize-plugin' ),
				'desc'      => esc_html__( 'Upload a M4V video.', 'socialize-plugin' ),
				'mode'      => false,
				'default' => '',
			),

			array(
				'id'        => 'video_mp4_url',
				'type'      => 'media',
				'title'     => esc_html__( 'MP4 Video', 'socialize-plugin' ),
				'desc'      => esc_html__( 'Upload a MP4 video.', 'socialize-plugin' ),
				'mode'      => false,
				'default' => '',
			),

			array(
				'id'        => 'video_webm_url',
				'type'      => 'media',
				'title'     => esc_html__( 'WebM Video', 'socialize-plugin' ),
				'desc'      => esc_html__( 'Upload a WebM video.', 'socialize-plugin' ),
				'mode'      => false,
				'default' => '',
			),
			
			array(
				'id'        => 'video_ogv_url',
				'type'      => 'media',
				'title'     => esc_html__( 'OGV Video', 'socialize-plugin' ),
				'desc'      => esc_html__( 'Upload a OGV video.', 'socialize-plugin' ),
				'mode'      => false,
				'default' => '',
			),

			array(
				'id'       => 'video_description',
				'type' => 'textarea',
				'title'    => esc_html__( 'Video Description', 'socialize-plugin' ),
				'desc'     => esc_html__( 'A description which is added next to your video.', 'socialize-plugin' ),
			),
					
		),
	);	
    $metaboxes[] = array(
        'id' => 'video-format-options',
        'title' => esc_html__( 'Video Options', 'socialize-plugin' ),
        'post_types' => array( 'post' ),
        'post_format' => array( 'video' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $video_format_options,
    ); 
       
       	
    // Main Post Options
    	
	$post_options = array();
    $post_options[] = array(
		'title' => esc_html__( 'General', 'socialize-plugin' ),		
		'desc' => esc_html__( 'By default most of these options are set from the Theme Options page to change all pages at once, but you can overwrite these options here so this page has different settings.', 'socialize-plugin' ),
		'icon' => 'el-icon-cogs',
		'fields' => array(

			array( 
				'id' => 'post_page_header',
				'title' => esc_html__( 'Page Header', 'socialize-plugin' ),
				'type' => 'select',
				'desc' => esc_html__( 'The page header on the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'gp-standard-page-header' => esc_html__( 'Standard', 'socialize-plugin' ),
					'gp-large-page-header' => esc_html__( 'Large', 'socialize-plugin' ),
					'gp-fullwidth-page-header' => esc_html__( 'Fullwidth', 'socialize-plugin' ),
					'gp-full-page-page-header' => esc_html__( 'Full Page', 'socialize-plugin' ),
				),
				'default' => 'default',
			),
				
			array(
				'id' => 'post_page_header_bg', 
				'title' => esc_html__( 'Page Header Image Background', 'socialize-plugin' ),
				'type'      => 'media',		
				'mode'      => false,	
				'required' => array( 'post_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The background of the page header.', 'socialize-plugin' ),
				'default' => '',
			),	

			array(
				'id' => 'post_page_header_text', 
				'title' => esc_html__( 'Page Header Text', 'socialize-plugin' ),
				'type'      => 'text',	
				'required' => array( 'post_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The text in the page header.', 'socialize-plugin' ),
				'default' => '',
			),	
								
			array(
				'id' => 'post_page_header_teaser_video_bg', 
				'title' => esc_html__( 'Title Header Teaser Video Background', 'socialize-plugin' ),
				'required' => array( 'post_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports HTML5 video only. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',
				'desc' => esc_html__( 'Video URL to the teaser video that is displayed in the title header.', 'socialize-plugin' ),
				'default' => '',
			),	

			array(
				'id' => 'post_page_header_full_video_bg', 
				'title' => esc_html__( 'Title Header Full Video Background', 'socialize-plugin' ),
				'required' => array( 'post_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports YouTube, Vimeo and HTML5 video. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',	
				'desc' => esc_html__( 'Video URL to the full video that is displayed when the play button is clicked.', 'socialize-plugin' ),
				'default' => '',
			),
			
			array( 
				'id' => 'post_title',
				'title' => esc_html__( 'Page Title', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Display the page title.', 'socialize-plugin' ),
				'options' => array(
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'enabled',
			),
						        
			array( 
				'id' => 'post_custom_title',
				'title' => esc_html__( 'Custom Title', 'socialize-plugin' ),
				'type' => 'text',
				'desc' => esc_html__( 'A custom title that overwrites the default title.', 'socialize-plugin' ),
				'default' => '',
			),
									
			array( 
				'id' => 'post_subtitle',
				'title' => esc_html__( 'Post Subtitle', 'socialize-plugin' ),
				'type' => 'textarea',
				'desc' => esc_html__( 'Add a subtitle below the title header.', 'socialize-plugin' ),
				'default' => '',
			),
		
			array( 
				'id' => 'post_layout',
				'title' => esc_html__( 'Page Layout', 'socialize-plugin' ),					
				'type' => 'image_select',
				'desc' => esc_html__( 'The layout of the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => array('title' => esc_html__( 'Default', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-left-sidebar' => array('title' => esc_html__( 'Left Sidebar', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-right-sidebar' => array('title' => esc_html__( 'Right Sidebar', 'socialize-plugin' ),  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
					'gp-both-sidebars' => array( 'title' => esc_html__( 'Both Sidebars', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/both-sidebars.png' ),
					'gp-no-sidebar' => array('title' => esc_html__( 'No Sidebar', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/no-sidebar.png'),
					'gp-fullwidth' => array('title' => esc_html__( 'Fullwidth', 'socialize-plugin' ), 'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
				),	
				'default' => 'default',
			),
			
			array(
				'id'      => 'post_left_sidebar',
				'type'    => 'select',
				'required' => array( 'post_layout', '=', array( 'gp-left-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Left Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),

			array(
				'id'      => 'post_right_sidebar',
				'type'    => 'select',
				'required' => array( 'post_layout', '=', array( 'gp-right-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Right Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),		

		),
	);

	$post_options[] = array(
		'title' => esc_html__( 'Image', 'socialize-plugin' ),
		'desc' => esc_html__( 'By default most of these options are set from the Theme Options page to change all pages at once, but you can overwrite these options here so this page has different settings.', 'socialize-plugin' ),
		'icon' => 'el-icon-picture',
		'fields' => array(  
	
			array(  
				'id' => 'post_featured_image',
				'title' => esc_html__( 'Featured Image', 'socialize-plugin' ),
				'type' => 'button_set',
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'default',
			),

			array(
				'id' => 'post_image',
				'type' => 'dimensions',
				'required'  => array( 'post_featured_image', '!=', 'disabled' ),
				'units' => false,
				'title' => esc_html__( 'Image Dimensions', 'socialize-plugin' ),
				'subtitle' => esc_html__( 'Set height to 0 to have a proportionate height.', 'socialize-plugin' ),
				'desc' => esc_html__( 'The width and height of the featured image.', 'socialize-plugin' ),
				'default'           => array(
					'width'     => '', 
					'height'    => '',
				),
			),

			array(
				'id' => 'post_hard_crop',
				'title' => esc_html__( 'Hard Crop', 'socialize-plugin' ),
				'type' => 'button_set',
				'required'  => array( 'post_featured_image', '!=', 'disabled' ),
				'desc' => esc_html__( 'Images are cropped even if it is smaller than the dimensions you want to crop it to.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'default',
			),

			array(
				'id' => 'post_image_alignment',
				'title' => esc_html__( 'Image Alignment', 'socialize-plugin' ),
				'type' => 'select',
				'required'  => array( 'post_featured_image', '!=', 'disabled' ),
				'desc' => esc_html__( 'Choose how the image aligns with the content.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'gp-image-wrap-left' => esc_html__( 'Left Wrap', 'socialize-plugin' ),
					'gp-image-wrap-right' => esc_html__( 'Right Wrap', 'socialize-plugin' ),
					'gp-image-above' => esc_html__( 'Above Content', 'socialize-plugin' ),
					'gp-image-align-left' => esc_html__( 'Left Align', 'socialize-plugin' ),
					'gp-image-align-right' => esc_html__( 'Right Align', 'socialize-plugin' ),
				),
				'default' => 'default',
			),
		
		),
	);	
    $metaboxes[] = array(
        'id' => 'post-options',
        'title' => esc_html__( 'Post Options', 'socialize-plugin' ),
        'post_types' => array( 'post' ),
        'post_format' => array( '0', 'audio', 'gallery', 'quote', 'video' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $post_options
    ); 
        
 
	/*--------------------------------------------------------------
	Page Options
	--------------------------------------------------------------*/	

	$page_options = array();
    $page_options[] = array(
		'title' => esc_html__( 'General', 'socialize-plugin' ),		
		'desc' => esc_html__( 'By default most of these options are set from the Theme Options page to change all pages at once, but you can overwrite these options here so this page has different settings.', 'socialize-plugin' ),
		'icon' => 'el-icon-cogs',
		'fields' => array(

			array( 
				'id' => 'page_page_header',
				'title' => esc_html__( 'Page Header', 'socialize-plugin' ),
				'type' => 'select',
				'desc' => esc_html__( 'The page header on the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'gp-standard-page-header' => esc_html__( 'Standard', 'socialize-plugin' ),
					'gp-large-page-header' => esc_html__( 'Large', 'socialize-plugin' ),
					'gp-fullwidth-page-header' => esc_html__( 'Fullwidth', 'socialize-plugin' ),
					'gp-full-page-page-header' => esc_html__( 'Full Page', 'socialize-plugin' ),
				),
				'default' => 'default',
			),
	
			array(
				'id' => 'page_page_header_bg', 
				'title' => esc_html__( 'Page Header Image Background', 'socialize-plugin' ),
				'type'      => 'media',		
				'mode'      => false,	
				'required' => array( 'page_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The background of the page header.', 'socialize-plugin' ),
				'default' => '',
			),
				
			array(
				'id' => 'page_page_header_text', 
				'title' => esc_html__( 'Page Header Text', 'socialize-plugin' ),
				'type'      => 'text',	
				'required' => array( 'page_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The text in the page header.', 'socialize-plugin' ),
				'default' => '',
			),
								
			array(
				'id' => 'page_page_header_teaser_video_bg', 
				'title' => esc_html__( 'Title Header Teaser Video Background', 'socialize-plugin' ),	
				'required' => array( 'page_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports HTML5 video only. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',
				'desc' => esc_html__( 'Video URL to the teaser video that is displayed in the title header.', 'socialize-plugin' ),
				'default' => '',
			),	

			array(
				'id' => 'page_page_header_full_video_bg', 
				'title' => esc_html__( 'Title Header Full Video Background', 'socialize-plugin' ),	
				'required' => array( 'page_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports YouTube, Vimeo and HTML5 video. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',	
				'desc' => esc_html__( 'Video URL to the full video that is displayed when the play button is clicked.', 'socialize-plugin' ),
				'default' => '',
			),
						
			array( 
				'id' => 'page_title',
				'title' => esc_html__( 'Page Title', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Display the page title.', 'socialize-plugin' ),
				'options' => array(
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'enabled',
			),
						        
			array( 
				'id' => 'page_custom_title',
				'title' => esc_html__( 'Custom Title', 'socialize-plugin' ),
				'type' => 'text',
				'desc' => esc_html__( 'A custom title that overwrites the default title.', 'socialize-plugin' ),
				'default' => '',
			),
									
			array( 
				'id' => 'page_subtitle',
				'title' => esc_html__( 'Page Subtitle', 'socialize-plugin' ),
				'type' => 'textarea',
				'desc' => esc_html__( 'Add a subtitle below the title header.', 'socialize-plugin' ),
				'default' => '',
			),
											
			array( 
				'id' => 'page_layout',
				'title' => esc_html__( 'Page Layout', 'socialize-plugin' ),					
				'type' => 'image_select',
				'desc' => esc_html__( 'The layout of the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => array('title' => esc_html__( 'Default', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/1c.png'),
					'gp-left-sidebar' => array('title' => esc_html__( 'Left Sidebar', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-right-sidebar' => array('title' => esc_html__( 'Right Sidebar', 'socialize-plugin' ),  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
					'gp-both-sidebars' => array( 'title' => esc_html__( 'Both Sidebars', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/both-sidebars.png' ),
					'gp-no-sidebar' => array('title' => esc_html__( 'No Sidebar', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/no-sidebar.png'),
					'gp-fullwidth' => array('title' => esc_html__( 'Fullwidth', 'socialize-plugin' ), 'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
				),	
				'default' => 'default',
			),
			
			array(
				'id'      => 'page_left_sidebar',
				'type'    => 'select',
				'required' => array( 'page_layout', '=', array( 'gp-left-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Left Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),

			array(
				'id'      => 'page_right_sidebar',
				'type'    => 'select',
				'required' => array( 'page_layout', '=', array( 'gp-right-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Right Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),
						
		),		
	);	

    $page_options[] = array(
		'title' => esc_html__( 'Image', 'socialize-plugin' ),
		'desc' => esc_html__( 'By default most of these options are set from the Theme Options page to change all pages at once, but you can overwrite these options here so this page has different settings.', 'socialize-plugin' ),
		'icon' => 'el-icon-picture',
		'fields' => array(
		
			array(  
				'id' => 'page_featured_image',
				'title' => esc_html__( 'Featured Image', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Shows the featured image on the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'default',
			),

			array(
				'id' => 'page_image',
				'type' => 'dimensions',
				'required'  => array( 'page_featured_image', '!=', 'disabled' ),
				'units' => false,
				'title' => esc_html__( 'Image Dimensions', 'socialize-plugin' ),
				'subtitle' => esc_html__( 'Set height to 0 to have a proportionate height.', 'socialize-plugin' ),
				'desc' => esc_html__( 'The width and height of the featured image.', 'socialize-plugin' ),
				'default'           => array(
					'width'     => '', 
					'height'    => '',
				),
			),

			array(
				'id' => 'page_hard_crop',
				'title' => esc_html__( 'Hard Crop', 'socialize-plugin' ),
				'type' => 'button_set',
				'required'  => array( 'page_featured_image', '!=', 'disabled' ),
				'desc' => esc_html__( 'Images are cropped even if it is smaller than the dimensions you want to crop it to.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'default',
			),

			array(
				'id' => 'page_image_alignment',
				'title' => esc_html__( 'Image Alignment', 'socialize-plugin' ),
				'type' => 'select',
				'required'  => array( 'page_featured_image', '!=', 'disabled' ),
				'desc' => esc_html__( 'Choose how the image aligns with the content.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'gp-image-wrap-left' => esc_html__( 'Left Wrap', 'socialize-plugin' ),
					'gp-image-wrap-right' => esc_html__( 'Right Wrap', 'socialize-plugin' ),
					'gp-image-above' => esc_html__( 'Above Content', 'socialize-plugin' ),
					'gp-image-align-left' => esc_html__( 'Left Align', 'socialize-plugin' ),
					'gp-image-align-right' => esc_html__( 'Right Align', 'socialize-plugin' ),
				),
				'default' => 'default',
			),	
			
		),
	);	
    $metaboxes[] = array(
        'id' => 'page-options',
        'title' => esc_html__( 'Page Options', 'socialize-plugin' ),
        'post_types' => array( 'page' ),
        'page_template' => array( 'default' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $page_options,
    ); 


	/*--------------------------------------------------------------
	Blog Page Template Options
	--------------------------------------------------------------*/	

    $blog_template_options = array();
    $blog_template_options[] = array(
		'title' => esc_html__( 'Blog', 'socialize-plugin' ),
		'icon' => 'el-icon-folder',
		'fields' => array(
			        
			array(
				'id'       => 'blog_template_cats',
				'type'     => 'select',
				'multi' => true,
				'title'    => esc_html__( 'Categories', 'socialize-plugin' ),
				'data' => 'terms',
				'args' => array( 'taxonomies' => array( 'category' ) ),
				'desc' => esc_html__( 'Select the categories you want to display.', 'socialize-plugin' ),
				'default' => '',
			),
			
			array( 
				'id' => 'blog_template_post_types',
				'title' => esc_html__( 'Post Types', 'socialize-plugin' ),
				'desc' => esc_html__( 'Select the post types you want to display.', 'socialize-plugin' ),
				'type' => 'select',
				'multi' => true,				
				'options' => array(
					'post' => esc_html__( 'Post', 'socialize-plugin' ),
					'page' => esc_html__( 'Page', 'socialize-plugin' ),
				),
				'default' => array( 'post' ),
			),
													
			array( 
				'id' => 'blog_template_format',
				'title' => esc_html__( 'Format', 'socialize-plugin' ),
				'type' => 'select',
				'desc' => esc_html__( 'The format to display the items in.', 'socialize-plugin' ),
				'options' => array(
					'gp-blog-large' => esc_html__( 'Large', 'socialize-plugin' ),
					'gp-blog-standard' => esc_html__( 'Standard', 'socialize-plugin' ),
					'gp-blog-columns-1' => esc_html__( '1 Column', 'socialize-plugin' ),
					'gp-blog-columns-2' => esc_html__( '2 Columns', 'socialize-plugin' ),
					'gp-blog-columns-3' => esc_html__( '3 Columns', 'socialize-plugin' ),
					'gp-blog-columns-4' => esc_html__( '4 Columns', 'socialize-plugin' ),
					'gp-blog-columns-5' => esc_html__( '5 Columns', 'socialize-plugin' ),
					'gp-blog-columns-6' => esc_html__( '6 Columns', 'socialize-plugin' ),
					'gp-blog-masonry' => esc_html__( 'Masonry', 'socialize-plugin' ),
				),
				'default' => 'gp-blog-large',
			),

			array(  
				'id' => 'blog_template_orderby',
				'title' => esc_html__( 'Order By', 'socialize-plugin' ),
				'type' => 'radio',
				'desc' => esc_html__( 'The criteria which the items are ordered by.', 'socialize-plugin' ),
				'options' => array(
					'newest' => esc_html__( 'Newest', 'socialize-plugin' ),
					'oldest' => esc_html__( 'Oldest', 'socialize-plugin' ),
					'title_az' => esc_html__( 'Title (A-Z)', 'socialize-plugin' ),
					'title_za' => esc_html__( 'Title (Z-A)', 'socialize-plugin' ),
					'comment_count' => esc_html__( 'Most Comments', 'socialize-plugin' ),
					'views' => esc_html__( 'Most Views', 'socialize-plugin' ),
					'menu_order' => esc_html__( 'Menu Order', 'socialize-plugin' ),
					'rand' => esc_html__( 'Random', 'socialize-plugin' ),
				),
				'default' => 'newest',
			),
			
			array(  
				'id' => 'blog_template_date_posted',
				'title' => esc_html__( 'Date Posted', 'socialize-plugin' ),
				'type' => 'radio',
				'desc' => esc_html__( 'The date the items were posted.', 'socialize-plugin' ),
				'options' => array(
					'all' => esc_html__( 'Any date', 'socialize-plugin' ),
					'year' => esc_html__( 'In the last year', 'socialize-plugin' ),
					'month' => esc_html__( 'In the last month', 'socialize-plugin' ),
					'week' => esc_html__( 'In the last week', 'socialize-plugin' ),
					'day' => esc_html__( 'In the last day', 'socialize-plugin' ),
				),
				'default' => 'all',
			),

			array(  
				'id' => 'blog_template_date_modified',
				'title' => esc_html__( 'Date Modified', 'socialize-plugin' ),
				'type' => 'radio',
				'desc' => esc_html__( 'The date the items were modified.', 'socialize-plugin' ),
				'options' => array(
					'all' => esc_html__( 'Any date', 'socialize-plugin' ),
					'year' => esc_html__( 'In the last year', 'socialize-plugin' ),
					'month' => esc_html__( 'In the last month', 'socialize-plugin' ),
					'week' => esc_html__( 'In the last week', 'socialize-plugin' ),
					'day' => esc_html__( 'In the last day', 'socialize-plugin' ),
				),
				'default' => 'all',
			),
								
			array(  
				'id' => 'blog_template_filter',
				'title' => esc_html__( 'Filter', 'socialize-plugin' ),
				'desc' => esc_html__( 'Add a dropdown filter menu to the page.', 'socialize-plugin' ),
				'type' => 'button_set',
				'options' => array(
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'enabled',
			),

			array(
				'id'        => 'blog_template_filter_options',
				'type'      => 'checkbox',
				'required'  => array( 'blog_template_filter', '=', 'enabled' ),
				'title'     => esc_html__( 'Filter Options', 'socialize-plugin' ),
				'desc' => esc_html__( 'Choose what options to display in the dropdown filter menu.', 'socialize-plugin' ), 
				'options'   => array(
					'cats' => esc_html__( 'Categories', 'socialize-plugin' ),
					'date' => esc_html__( 'Date', 'socialize-plugin' ),
					'title' => esc_html__( 'Title', 'socialize-plugin' ),
					'comment_count' => esc_html__( 'Comment Count', 'socialize-plugin' ),
					'views' => esc_html__( 'Views', 'socialize-plugin' ),
					'date_posted' => esc_html__( 'Date Posted', 'socialize-plugin' ),
					'date_modified' => esc_html__( 'Date Modified', 'socialize-plugin' ),
				),
				'default'   => array(
					'cats' => 0,
					'date' => '1',
					'title' => '1',
					'comment_count' => '1',
					'views' => '1',
					'date_posted' => '1',
					'date_modified' => '0',
				)
			),

			array(
				'id'       => 'blog_template_filter_cats_id',
				'type'     => 'select',
				'required'  => array( 'blog_template_filter', '=', 'enabled' ),
				'title'    => esc_html__( 'Filter Category', 'socialize-plugin' ),
				'data' => 'terms',
				'args' => array( 'taxonomies' => array( 'category' ) ),
				'desc' => esc_html__( 'Select the category you want to filter by, leave blank to display all categories.', 'socialize-plugin' ),
				'subtitle' => esc_html__( 'The sub categories of this category will also be displayed.', 'socialize-plugin' ),
				'default' => '',
			),
			                    										
			array(
				'id'       => 'blog_template_per_page',
				'type'     => 'spinner',
				'title'    => esc_html__( 'Items Per Page', 'socialize-plugin' ),
				'desc' => esc_html__( 'The number of items on each page.', 'socialize-plugin' ),
				'min' => 1,
				'max' => 999999,
				'default' => 12,
			),
												
			array( 
				'id' => 'blog_template_content_display',
				'title' => esc_html__( 'Content Display', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'The amount of content displayed.', 'socialize-plugin' ),
				'options' => array(
					'excerpt' => esc_html__( 'Excerpt', 'socialize-plugin' ),
					'full_content' => esc_html__( 'Full Content', 'socialize-plugin' ),
				),
				'default' => 'excerpt',
			),
		
			array( 
				'id' => 'blog_template_excerpt_length',
				'title' => esc_html__( 'Excerpt Length', 'socialize-plugin' ),
				'required'  => array( 'blog_template_content_display', '=', 'excerpt' ),
				'type' => 'spinner',
				'desc' => esc_html__( 'The number of characters in excerpts.', 'socialize-plugin' ),
				'min' => 0,
				'max' => 999999,
				'default' => '800',
			),

			array(
				'id'        => 'blog_template_meta',
				'type'      => 'checkbox',
				'title'     => esc_html__( 'Post Meta', 'socialize-plugin' ),
				'desc' => esc_html__( 'Select the meta data you want to display.', 'socialize-plugin' ), 
				'options'   => array(
					'author' => esc_html__( 'Author Name', 'socialize-plugin' ),
					'date' => esc_html__( 'Post Date', 'socialize-plugin' ),
					'comment_count' => esc_html__( 'Comment Count', 'socialize-plugin' ),
                    'views' => esc_html__( 'Views', 'socialize-plugin' ),
					'cats' => esc_html__( 'Categories', 'socialize-plugin' ),
					'tags' => esc_html__( 'Post Tags', 'socialize-plugin' ),
				),
				'default'   => array(
					'author' => '1',
					'date' => '1', 
					'comment_count' => '1',
					'views' => '1',
					'cats' => '1',
					'tags' => '0',
				),
			),
									
			array(  
				'id' => 'blog_template_read_more_link',
				'title' => esc_html__( 'Read More Link', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Add a read more link below the content.', 'socialize-plugin' ),
				'options' => array(
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'disabled',
			),
								
		),
	);
	
    $blog_template_options[] = array(
		'title' => esc_html__( 'General', 'socialize-plugin' ),
		'icon' => 'el-icon-cogs',
		'fields' => array(
		
			array( 
				'id' => 'blog_template_page_header',
				'title' => esc_html__( 'Page Header', 'socialize-plugin' ),
				'type' => 'select',
				'desc' => esc_html__( 'Display the title header on the page.', 'socialize-plugin' ),
				'options' => array(
					'gp-standard-page-header' => esc_html__( 'Standard', 'socialize-plugin' ),
					'gp-large-page-header' => esc_html__( 'Large', 'socialize-plugin' ),
					'gp-fullwidth-page-header' => esc_html__( 'Fullwidth', 'socialize-plugin' ),
					'gp-full-page-page-header' => esc_html__( 'Full Page', 'socialize-plugin' ),
				),
				'default' => 'gp-fullwidth-page-header',
			),
										
			array(
				'id'        => 'blog_template_page_header_bg',
				'type'      => 'media',
				'mode'      => false,	
				'required' => array( 'blog_template_page_header', '!=', 'gp-standard-page-header' ),
				'title'     => esc_html__( 'Page Header Image Background', 'socialize-plugin' ),
				'desc' => esc_html__( 'The background of the page header.', 'socialize-plugin' ),
				'default' => '',
			),

			array(
				'id' => 'blog_template_page_header_text', 
				'title' => esc_html__( 'Page Header Text', 'socialize-plugin' ),
				'type'      => 'text',	
				'required' => array( 'blog_template_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The text in the page header.', 'socialize-plugin' ),
				'default' => '',
			),
						
			array(
				'id' => 'blog_template_page_header_teaser_video_bg', 
				'title' => esc_html__( 'Title Header Teaser Video Background', 'socialize-plugin' ),	
				'required' => array( 'blog_template_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports HTML5 video only. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',
				'desc' => esc_html__( 'Video URL to the teaser video that is displayed in the title header.', 'socialize-plugin' ),
				'default' => '',
			),	

			array(
				'id' => 'blog_template_page_header_full_video_bg', 
				'title' => esc_html__( 'Title Header Full Video Background', 'socialize-plugin' ),
				'required' => array( 'blog_template_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports YouTube, Vimeo and HTML5 video. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',	
				'desc' => esc_html__( 'Video URL to the full video that is displayed when the play button is clicked.', 'socialize-plugin' ),
				'default' => '',
			),
						
			array( 
				'id' => 'blog_template_title',
				'title' => esc_html__( 'Page Title', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Display the page title.', 'socialize-plugin' ),
				'options' => array(
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'enabled',
			),
						
			array( 
				'id' => 'blog_template_custom_title',
				'title' => esc_html__( 'Custom Title', 'socialize-plugin' ),
				'type' => 'text',
				'desc' => esc_html__( 'A custom title that overwrites the default title.', 'socialize-plugin' ),
				'default' => '',
			),
						
			array( 
				'id' => 'blog_template_subtitle',
				'title' => esc_html__( 'Page Subtitle', 'socialize-plugin' ),
				'type' => 'textarea',
				'desc' => esc_html__( 'Add a subtitle below the title header.', 'socialize-plugin' ),
				'default' => '',
			),
					
			array( 
				'id' => 'blog_template_layout',
				'title' => esc_html__( 'Page Layout', 'socialize-plugin' ),
				'type' => 'image_select',
				'desc' => esc_html__( 'The layout of the page.', 'socialize-plugin' ),
				'options' => array(
					'gp-left-sidebar' => array('title' => esc_html__( 'Left Sidebar', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-right-sidebar' => array('title' => esc_html__( 'Right Sidebar', 'socialize-plugin' ),  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
					'gp-both-sidebars' => array( 'title' => esc_html__( 'Both Sidebars', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/both-sidebars.png' ),
					'gp-no-sidebar' => array('title' => esc_html__( 'No Sidebar', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/no-sidebar.png'),
					'gp-fullwidth' => array('title' => esc_html__( 'Fullwidth', 'socialize-plugin' ), 'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
				),	
				'default' => 'gp-right-sidebar',
			),
			
			array(
				'id'      => 'blog_template_left_sidebar',
				'type'    => 'select',
				'required' => array( 'blog_template_layout', '=', array( 'gp-left-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Left Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => 'gp-left-sidebar',
			),

			array(
				'id'      => 'blog_template_right_sidebar',
				'type'    => 'select',
				'required' => array( 'blog_template_layout', '=', array( 'gp-right-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Right Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => 'gp-right-sidebar',
			),
		
		),			
	);		
	
    $blog_template_options[] = array(
		'title' => esc_html__( 'Image', 'socialize-plugin' ),
		'icon' => 'el-icon-picture',
		'fields' => array(	
			
			array(  
				'id' => 'blog_template_featured_image',
				'title' => esc_html__( 'Featured Image', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Display the featured images..', 'socialize-plugin' ),
				'options' => array(
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'enabled',
			),

			array(
				'id' => 'blog_template_image',
				'type' => 'dimensions',
				'required'  => array( 'blog_template_featured_image', '!=', 'disabled' ),
				'units' => false,
				'title' => esc_html__( 'Image Dimensions', 'socialize-plugin' ),
				'desc' => esc_html__( 'The width and height of the featured images.', 'socialize-plugin' ),
				'subtitle' => esc_html__( 'Set height to 0 to have a proportionate height.', 'socialize-plugin' ),
				'default' => array(
					'width'     => 1050, 
					'height'    => 600,
				),
			),

			array(
				'id' => 'blog_template_hard_crop',
				'title' => esc_html__( 'Hard Crop', 'socialize-plugin' ),
				'type' => 'button_set',
				'required'  => array( 'blog_template_featured_image', '!=', 'disabled' ),
				'desc' => esc_html__( 'Images are cropped even if it is smaller than the dimensions you want to crop it to.', 'socialize-plugin' ),
				'options' => array(
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => true,
			),

			array(
				'id' => 'blog_template_image_alignment',
				'title' => esc_html__( 'Image Alignment', 'socialize-plugin' ),
				'type' => 'select',
				'required'  => array( 'blog_template_featured_image', '!=', 'disabled' ),
				'desc' => esc_html__( 'Choose how the image aligns with the content.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'gp-image-wrap-left' => esc_html__( 'Left Wrap', 'socialize-plugin' ),
					'gp-image-wrap-right' => esc_html__( 'Right Wrap', 'socialize-plugin' ),
					'gp-image-above' => esc_html__( 'Above Content', 'socialize-plugin' ),
					'gp-image-align-left' => esc_html__( 'Left Align', 'socialize-plugin' ),
					'gp-image-align-right' => esc_html__( 'Right Align', 'socialize-plugin' ),
				),
				'default' => 'gp-image-above',
			),

		),		
	);
    $metaboxes[] = array(
        'id' => 'blog-template-options',
        'title' => esc_html__( 'Blog Options', 'socialize-plugin' ),
        'post_types' => array( 'page' ),
        'page_template' => array( 'blog-template.php' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $blog_template_options,
    );
    

	/*--------------------------------------------------------------
	Custom Homepage Options
	--------------------------------------------------------------*/	

	$homepage_options = array();
    $homepage_options[] = array(
		'fields' => array(

			array( 
				'id' => 'homepage_page_header',
				'title' => esc_html__( 'Page Header', 'socialize-plugin' ),
				'type' => 'select',
				'desc' => esc_html__( 'The page header on the page.', 'socialize-plugin' ),
				'options' => array(
					'gp-standard-page-header' => esc_html__( 'Standard', 'socialize-plugin' ),
					'gp-large-page-header' => esc_html__( 'Large', 'socialize-plugin' ),
					'gp-fullwidth-page-header' => esc_html__( 'Fullwidth', 'socialize-plugin' ),
					'gp-full-page-page-header' => esc_html__( 'Full Page', 'socialize-plugin' ),
				),
				'default' => 'gp-standard-page-header',
			),
	
			array(
				'id' => 'homepage_page_header_bg', 
				'title' => esc_html__( 'Page Header Image Background', 'socialize-plugin' ),
				'type'      => 'media',		
				'mode'      => false,	
				'required' => array( 'homepage_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The background of the page header.', 'socialize-plugin' ),
				'default' => '',
			),
				
			array(
				'id' => 'homepage_page_header_text', 
				'title' => esc_html__( 'Page Header Text', 'socialize-plugin' ),
				'type'      => 'text',	
				'required' => array( 'page_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The text in the page header.', 'socialize-plugin' ),
				'default' => '',
			),
								
			array(
				'id' => 'homepage_page_header_teaser_video_bg', 
				'title' => esc_html__( 'Title Header Teaser Video Background', 'socialize-plugin' ),	
				'required' => array( 'homepage_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports HTML5 video only. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',
				'desc' => esc_html__( 'Video URL to the teaser video that is displayed in the title header.', 'socialize-plugin' ),
				'default' => '',
			),	

			array(
				'id' => 'homepage_page_header_full_video_bg', 
				'title' => esc_html__( 'Title Header Full Video Background', 'socialize-plugin' ),	
				'required' => array( 'homepage_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports YouTube, Vimeo and HTML5 video. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',	
				'desc' => esc_html__( 'Video URL to the full video that is displayed when the play button is clicked.', 'socialize-plugin' ),
				'default' => '',
			),
						
			array( 
				'id' => 'homepage_title',
				'title' => esc_html__( 'Page Title', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Display the page title.', 'socialize-plugin' ),
				'options' => array(
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'disabled',
			),
						        
			array( 
				'id' => 'homepage_custom_title',
				'title' => esc_html__( 'Custom Title', 'socialize-plugin' ),
				'type' => 'text',
				'desc' => esc_html__( 'A custom title that overwrites the default title.', 'socialize-plugin' ),
				'default' => '',
			),
									
			array( 
				'id' => 'homepage_subtitle',
				'title' => esc_html__( 'Page Subtitle', 'socialize-plugin' ),
				'type' => 'textarea',
				'desc' => esc_html__( 'Add a subtitle below the title header.', 'socialize-plugin' ),
				'default' => '',
			),
											
			array( 
				'id' => 'homepage_layout',
				'title' => esc_html__( 'Page Layout', 'socialize-plugin' ),					
				'type' => 'image_select',
				'desc' => esc_html__( 'The layout of the page.', 'socialize-plugin' ),
				'options' => array(
					'gp-left-sidebar' => array('title' => esc_html__( 'Left Sidebar', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-right-sidebar' => array('title' => esc_html__( 'Right Sidebar', 'socialize-plugin' ),  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
					'gp-both-sidebars' => array( 'title' => esc_html__( 'Both Sidebars', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/both-sidebars.png' ),
					'gp-no-sidebar' => array('title' => esc_html__( 'No Sidebar', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/no-sidebar.png'),
					'gp-fullwidth' => array('title' => esc_html__( 'Fullwidth', 'socialize-plugin' ), 'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
				),	
				'default' => 'gp-both-sidebars',
			),
			
			array(
				'id'      => 'homepage_left_sidebar',
				'type'    => 'select',
				'required' => array( 'homepage_layout', '=', array( 'gp-left-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Left Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => 'gp-homepage-left-sidebar',
			),

			array(
				'id'      => 'homepage_right_sidebar',
				'type'    => 'select',
				'required' => array( 'homepage_layout', '=', array( 'gp-right-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Right Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => 'gp-homepage-right-sidebar',
			),
			
			array(
				'id'       => 'homepage_content_header',
				'type'     => 'editor',
				'title'    => esc_html__( 'Content Header', 'socialize-plugin' ),
				'desc' => esc_html__( 'Add content directly above the page content and sidebar.', 'socialize-plugin' ),
				'default' => '[slider cats="" format="gp-slider-two-cols" per_page="3" slider_speed="0"]',
			),

			array(
				'id' => 'homepage_content_header_format',
				'title' => esc_html__( 'Content Header Format', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Choose whether the content area stretches across the entire page.', 'socialize-plugin' ),
				'options' => array(
					'fixed' => esc_html__( 'Fixed', 'socialize-plugin' ),
					'fullwidth' => esc_html__( 'Fullwidth', 'socialize-plugin' ),
				),
				'default' => 'fixed',
			),
						
		),		
	);	
	
    $metaboxes[] = array(
        'id' => 'homepage-options',
        'title' => esc_html__( 'Homepage Options', 'socialize-plugin' ),
        'post_types' => array( 'page' ),
        'page_template' => array( 'homepage-template.php' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $homepage_options,
    ); 
    
        	
	/*--------------------------------------------------------------
	Portfolio Page Template Options
	--------------------------------------------------------------*/	

    $portfolio_template_options = array();
    $portfolio_template_options[] = array(
		'title' => esc_html__( 'Portfolio', 'socialize-plugin' ),
		'icon' => 'el-icon-photo-alt',
		'fields' => array(	
			        
			array(
				'id'       => 'portfolio_template_cats',
				'type'     => 'select',
				'multi' => true,
				'title'    => esc_html__( 'Portfolio Categories', 'socialize-plugin' ),
				'data' => 'terms',
				'args' => array( 'taxonomies' => 'gp_portfolios' ),
				'desc' => esc_html__( 'Select the portfolio categories you want to display.', 'socialize-plugin' ),
				'default' => '',
			),	
	
			array( 
				'id' => 'portfolio_template_format',
				'title' => esc_html__( 'Format', 'socialize-plugin' ),					
				'type' => 'select',
				'desc' => esc_html__( 'The format to display the items in.', 'socialize-plugin' ),
				'options' => array(
					'gp-portfolio-columns-2' => esc_html__( '2 Columns', 'socialize-plugin' ),
					'gp-portfolio-columns-3' => esc_html__( '3 Columns', 'socialize-plugin' ),
					'gp-portfolio-columns-4' => esc_html__( '4 Columns', 'socialize-plugin' ),
					'gp-portfolio-columns-5' => esc_html__( '5 Columns', 'socialize-plugin' ),
					'gp-portfolio-columns-6' => esc_html__( '6 Columns', 'socialize-plugin' ),
					'gp-portfolio-masonry' => esc_html__( 'Masonry', 'socialize-plugin' ),
				),	
				'default' => 'gp-portfolio-columns-2',
			),

			array(  
				'id' => 'portfolio_template_orderby',
				'title' => esc_html__( 'Order By', 'socialize-plugin' ),
				'type' => 'radio',
				'desc' => esc_html__( 'The criteria which the items are ordered by.', 'socialize-plugin' ),
				'options' => array(
					'newest' => esc_html__( 'Newest', 'socialize-plugin' ),
					'oldest' => esc_html__( 'Oldest', 'socialize-plugin' ),
					'title_az' => esc_html__( 'Title (A-Z)', 'socialize-plugin' ),
					'title_za' => esc_html__( 'Title (Z-A)', 'socialize-plugin' ),
					'comment_count' => esc_html__( 'Most Comments', 'socialize-plugin' ),
					'views' => esc_html__( 'Most Views', 'socialize-plugin' ),
					'menu_order' => esc_html__( 'Menu Order', 'socialize-plugin' ),
					'rand' => esc_html__( 'Random', 'socialize-plugin' ),
				),
				'default' => 'newest',
			),

			array(  
				'id' => 'portfolio_template_date_posted',
				'title' => esc_html__( 'Date Posted', 'socialize-plugin' ),
				'type' => 'radio',
				'desc' => esc_html__( 'The date the items were posted.', 'socialize-plugin' ),
				'options' => array(
					'all' => esc_html__( 'Any date', 'socialize-plugin' ),
					'year' => esc_html__( 'In the last year', 'socialize-plugin' ),
					'month' => esc_html__( 'In the last month', 'socialize-plugin' ),
					'week' => esc_html__( 'In the last week', 'socialize-plugin' ),
					'day' => esc_html__( 'In the last day', 'socialize-plugin' ),
				),
				'default' => 'all',
			),

			array(  
				'id' => 'portfolio_template_date_modified',
				'title' => esc_html__( 'Date Modified', 'socialize-plugin' ),
				'type' => 'radio',
				'desc' => esc_html__( 'The date the items were modified.', 'socialize-plugin' ),
				'options' => array(
					'all' => esc_html__( 'Any date', 'socialize-plugin' ),
					'year' => esc_html__( 'In the last year', 'socialize-plugin' ),
					'month' => esc_html__( 'In the last month', 'socialize-plugin' ),
					'week' => esc_html__( 'In the last week', 'socialize-plugin' ),
					'day' => esc_html__( 'In the last day', 'socialize-plugin' ),
				),
				'default' => 'all',
			),		

			array(  
				'id' => 'portfolio_template_filter',
				'title' => esc_html__( 'Portfolio Filter', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Add category filter links to the page.', 'socialize-plugin' ),
				'options' => array(
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'enabled',
			),					

			array( 
				'id' => 'portfolio_template_per_page',
				'title' => esc_html__( 'Items Per Page', 'socialize-plugin' ),
				'type' => 'spinner',
				'desc' => esc_html__( 'The number of items on each page.', 'socialize-plugin' ),
				'min' => 0,
				'max' => 999999,
				'default' => 12,
			),
				
		)
	);
	
    $portfolio_template_options[] = array(
		'title' => esc_html__( 'General', 'socialize-plugin' ),	
		'desc' => esc_html__( 'By default most of these options are set from the Theme Options page to change all pages at once, but you can overwrite these options here so this page has different settings.', 'socialize-plugin' ),	
		'icon' => 'el-icon-cogs',
		'fields' => array(
				
			array( 
				'id' => 'portfolio_template_page_header',
				'title' => esc_html__( 'Page Header', 'socialize-plugin' ),
				'type' => 'select',
				'desc' => esc_html__( 'Display the title header on the page.', 'socialize-plugin' ),
				'options' => array(
					'gp-standard-page-header' => esc_html__( 'Standard', 'socialize-plugin' ),
					'gp-large-page-header' => esc_html__( 'Large', 'socialize-plugin' ),
					'gp-fullwidth-page-header' => esc_html__( 'Fullwidth', 'socialize-plugin' ),
					'gp-full-page-page-header' => esc_html__( 'Full Page', 'socialize-plugin' ),
				),
				'default' => 'gp-standard-page-header',
			),
							
			array(
				'id' => 'portfolio_template_page_header_bg', 
				'title' => esc_html__( 'Page Header Image Background', 'socialize-plugin' ),	
				'required' => array( 'portfolio_template_page_header', '!=', 'gp-standard-page-header' ),
				'type'      => 'media',	
				'mode'      => false,		
				'desc' => esc_html__( 'The background of the page header.', 'socialize-plugin' ),
				'default' => '',
			),
			
			array(
				'id' => 'portfolio_template_page_header_text', 
				'title' => esc_html__( 'Page Header Text', 'socialize-plugin' ),
				'type'      => 'text',	
				'required' => array( 'portfolio_template_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The text in the page header.', 'socialize-plugin' ),
				'default' => '',
			),
			
			array(
				'id' => 'portfolio_template_page_header_teaser_video_bg', 
				'title' => esc_html__( 'Title Header Teaser Video Background', 'socialize-plugin' ),	
				'required' => array( 'portfolio_template_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports HTML5 video only. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',
				'desc' => esc_html__( 'Video URL to the teaser video that is displayed in the title header.', 'socialize-plugin' ),
				'default' => '',
			),	

			array(
				'id' => 'portfolio_template_page_header_full_video_bg', 
				'title' => esc_html__( 'Title Header Full Video Background', 'socialize-plugin' ),	
				'required' => array( 'portfolio_template_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports YouTube, Vimeo and HTML5 video. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',	
				'desc' => esc_html__( 'Video URL to the full video that is displayed when the play button is clicked.', 'socialize-plugin' ),
				'default' => '',
			),
						
			array( 
				'id' => 'portfolio_template_title',
				'title' => esc_html__( 'Page Title', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Display the page title.', 'socialize-plugin' ),
				'options' => array(
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'enabled',
			),
						
			array( 
				'id' => 'portfolio_template_custom_title',
				'title' => esc_html__( 'Custom Title', 'socialize-plugin' ),
				'type' => 'text',
				'desc' => esc_html__( 'A custom title that overwrites the default title.', 'socialize-plugin' ),
				'default' => '',
			),
						
			array( 
				'id' => 'portfolio_template_subtitle',
				'title' => esc_html__( 'Page Subtitle', 'socialize-plugin' ),
				'type' => 'textarea',
				'desc' => esc_html__( 'Add a subtitle below the title header.', 'socialize-plugin' ),
				'default' => '',
			),
											
			array( 
				'id' => 'portfolio_template_layout',
				'title' => esc_html__( 'Page Layout', 'socialize-plugin' ),					
				'type' => 'image_select',
				'desc' => esc_html__( 'The layout of the page.', 'socialize-plugin' ),
				'options' => array(
					'gp-left-sidebar' => array('title' => esc_html__( 'Left Sidebar', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-right-sidebar' => array('title' => esc_html__( 'Right Sidebar', 'socialize-plugin' ),  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
					'gp-both-sidebars' => array( 'title' => esc_html__( 'Both Sidebars', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/both-sidebars.png' ),
					'gp-no-sidebar' => array('title' => esc_html__( 'No Sidebar', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/no-sidebar.png'),
					'gp-fullwidth' => array('title' => esc_html__( 'Fullwidth', 'socialize-plugin' ), 'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
				),	
				'default' => 'gp-no-sidebar',
			),
			
			array(
				'id'      => 'portfolio_template_left_sidebar',
				'type'    => 'select',
				'required' => array( 'portfolio_template_layout', '=', array( 'gp-left-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Left Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => 'gp-left-sidebar',
			),

			array(
				'id'      => 'portfolio_template_right_sidebar',
				'type'    => 'select',
				'required' => array( 'portfolio_template_layout', '=', array( 'gp-right-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Right Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => 'gp-right-sidebar',
			),
			 
		),	
	);
    $metaboxes[] = array(
        'id' => 'portfolio-template-options',
        'title' => esc_html__( 'Portfolio Options', 'socialize-plugin' ),
        'post_types' => array( 'page' ),
        'page_template' => array( 'portfolio-template.php' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $portfolio_template_options,
    );
    
    
	/*--------------------------------------------------------------
	Link Page Template Options
	--------------------------------------------------------------*/	

    $link_template_options = array();
    $link_template_options[] = array(
        'fields' => array(
        
			array( 
				'id' => 'link_template_link',
				'title' => esc_html__( 'Link', 'socialize-plugin' ),
				'type' => 'text',
				'desc' => esc_html__( 'The link which your page goes to.', 'socialize-plugin' ),
				'default' => '',
				'validate' => 'url',
			),

			array( 
				'id' => 'link_template_link_target',
				'title' => esc_html__( 'Link Target', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'The target for the link.', 'socialize-plugin' ),
				'options' => array(
					'_blank' => esc_html__( 'New Window', 'socialize-plugin' ),
					'_self' => esc_html__( 'Same Window', 'socialize-plugin' ),
				),
				'default' => '_self',
			),
															 
		),
	);	
    $metaboxes[] = array(
        'id' => 'link-options',
        'title' => esc_html__( 'Link Options', 'socialize-plugin' ),
        'post_types' => array( 'page' ),
        'page_template' => array( 'link-template.php' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $link_template_options,
    );
  
               
	/*--------------------------------------------------------------
	Portfolio Item Options
	--------------------------------------------------------------*/	

    $portfolio_item_options = array();
    $portfolio_item_options[] = array(
		'title' => esc_html__( 'Portfolio', 'socialize-plugin' ),	
		'desc' => esc_html__( 'By default most of these options are set from the Theme Options page to change all pages at once, but you can overwrite these options here so this page has different settings.', 'socialize-plugin' ),	
		'icon' => 'el-icon-photo-alt',
        'fields' => array(

			array(
				'id'        => 'portfolio_item_type',
				'type'      => 'radio',
				'title'     => esc_html__( 'Image/Slider Type', 'socialize-plugin' ),
				'desc' => esc_html__( 'The type of image or slider on the page.', 'socialize-plugin' ),
				'options'   => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'gp-left-image' => 'Left Featured Image',
					'gp-fullwidth-image' => 'Fullwidth Featured Image',
					'gp-left-slider' => 'Left Slider',
					'gp-fullwidth-slider' => 'Fullwidth Slider',
					'none' => 'None',
				), 
				'default'   => 'default',
			),   

			array(
				'id'        => 'portfolio_item_gallery_slider',
				'type'      => 'gallery',
				'required'  => array( 'portfolio_item_type', '=', array( 'gp-left-slider', 'gp-fullwidth-slider' ) ),
				'title'     => esc_html__( 'Gallery Slider', 'socialize-plugin' ),
				'subtitle'  => esc_html__( 'Create a new gallery by selecting an existing one or uploading new images using the WordPress native uploader.', 'socialize-plugin' ),
				'desc'  => esc_html__( 'Add a gallery slider.', 'socialize-plugin' ),
				'default' => '',
			),
			
			array(
				'id' => 'portfolio_item_image',
				'type' => 'dimensions',
				'required'  => array( 'portfolio_item_type', '!=', 'none' ),
				'units' => false,
				'title' => esc_html__( 'Image/Slider Dimensions', 'socialize-plugin' ),
				'subtitle' => esc_html__( 'Set height to 0 to have a proportionate height.', 'socialize-plugin' ),
				'desc' => esc_html__( 'The width and height of the featured image or slider.', 'socialize-plugin' ),
				'default'           => array(
					'width'     => '', 
					'height'    => '',
				),
			),
			
			array(
				'id' => 'portfolio_item_hard_crop',
				'title' => esc_html__( 'Hard Crop', 'socialize-plugin' ),
				'type' => 'button_set',
				'required'  => array( 'portfolio_item_type', '!=', 'none' ),
				'desc' => esc_html__( 'Images are cropped even if it is smaller than the dimensions you want to crop it to.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'default',
			),

			array(
				'id' => 'portfolio_item_image_size',
				'title' => esc_html__( 'Image Size', 'socialize-plugin' ),
				'subtitle' => esc_html__( 'Only for use with the Masonry portfolio type.', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Size of the image when displayed on a masonry portfolio page.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'gp-regular' => esc_html__( 'Regular', 'socialize-plugin' ),
					'gp-narrow' => esc_html__( 'Narrow', 'socialize-plugin' ),
					'gp-tall' => esc_html__( 'Tall', 'socialize-plugin' ),
				),
				'default' => 'default',
			),
		
			array( 	
				'id' => 'portfolio_item_link',
				'title' => esc_html__( 'Button Link', 'socialize-plugin' ),
				'type' => 'text',
				'desc' => esc_html__( 'The link for the button.', 'socialize-plugin' ),
				'validate' => 'url',
				'default' => '',
			), 
								
			array( 	
				'id' => 'portfolio_item_link_text',
				'title' => esc_html__( 'Button Text', 'socialize-plugin' ),
				'type' => 'text',
				'desc' => esc_html__( 'The text for the button.', 'socialize-plugin' ),
				'default' => '',
			), 

			array( 
				'id' => 'portfolio_item_link_target',
				'title' => esc_html__( 'Button Link Target', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'The target for the button link.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'_blank' => esc_html__( 'New Window', 'socialize-plugin' ),
					'_self' => esc_html__( 'Same Window', 'socialize-plugin' ),
				),
				'default' => 'default',
			),
			
		),
	);		
	
    $portfolio_item_options[] = array(
		'title' => esc_html__( 'General', 'socialize-plugin' ),	
		'desc' => esc_html__( 'By default most of these options are set from the Theme Options page to change all pages at once, but you can overwrite these options here so this page has different settings.', 'socialize-plugin' ),	
		'icon' => 'el-icon-cogs',
        'fields' => array(
        
			array( 
				'id' => 'portfolio_item_page_header',
				'title' => esc_html__( 'Page Header', 'socialize-plugin' ),
				'type' => 'select',
				'desc' => esc_html__( 'The page header on the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'gp-standard-page-header' => esc_html__( 'Standard', 'socialize-plugin' ),
					'gp-large-page-header' => esc_html__( 'Large', 'socialize-plugin' ),
					'gp-fullwidth-page-header' => esc_html__( 'Fullwidth', 'socialize-plugin' ),
					'gp-full-page-page-header' => esc_html__( 'Full Page', 'socialize-plugin' ),
				),
				'default' => 'default',
			),
									
			array(
				'id' => 'portfolio_item_page_header_bg', 
				'title' => esc_html__( 'Page Header Image Background', 'socialize-plugin' ),
				'required' => array( 'portfolio_item_page_header', '!=', 'gp-standard-page-header' ),
				'type'      => 'media',
				'mode'      => false,
				'desc' => esc_html__( 'The background of the page header.', 'socialize-plugin' ),
				'default' => '',
			),
			
			array(
				'id' => 'portfolio_item_page_header_text', 
				'title' => esc_html__( 'Page Header Text', 'socialize-plugin' ),
				'type'      => 'text',	
				'required' => array( 'portfolio_item_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The text in the page header.', 'socialize-plugin' ),
				'default' => '',
			),
			
			array(
				'id' => 'portfolio_item_page_header_teaser_video_bg', 
				'title' => esc_html__( 'Title Header Teaser Video Background', 'socialize-plugin' ),
				'required' => array( 'portfolio_item_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports HTML5 video only. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',
				'desc' => esc_html__( 'Video URL to the teaser video that is displayed in the title header.', 'socialize-plugin' ),
				'default' => '',
			),	

			array(
				'id' => 'portfolio_item_page_header_full_video_bg', 
				'title' => esc_html__( 'Title Header Full Video Background', 'socialize-plugin' ),
				'required' => array( 'portfolio_item_page_header', '!=', 'gp-standard-page-header' ),
				'subtitle' => esc_html__( 'Supports YouTube, Vimeo and HTML5 video. For multiple HTML5 formats, each video should have exactly the same filename but remove the extension (e.g. .mp4) from the filename in the text box.', 'socialize-plugin' ),
				'type'      => 'text',	
				'validate'  => 'url',	
				'desc' => esc_html__( 'Video URL to the full video that is displayed when the play button is clicked.', 'socialize-plugin' ),
				'default' => '',
			),
						
			array( 
				'id' => 'portfolio_item_title',
				'title' => esc_html__( 'Page Title', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'Display the page title.', 'socialize-plugin' ),
				'options' => array(
					'enabled' => esc_html__( 'Enabled', 'socialize-plugin' ),
					'disabled' => esc_html__( 'Disabled', 'socialize-plugin' ),
				),
				'default' => 'enabled',
			),
						 
			array( 
				'id' => 'portfolio_item_custom_title',
				'title' => esc_html__( 'Custom Title', 'socialize-plugin' ),
				'type' => 'text',
				'desc' => esc_html__( 'A custom title that overwrites the default title.', 'socialize-plugin' ),
				'default' => '',
			),
			
			array( 
				'id' => 'portfolio_item_subtitle',
				'title' => esc_html__( 'Page Subtitle', 'socialize-plugin' ),
				'type' => 'textarea',
				'desc' => esc_html__( 'Add a subtitle below the title header.', 'socialize-plugin' ),
			),
											
			array( 
				'id' => 'portfolio_item_layout',
				'title' => esc_html__( 'Page Layout', 'socialize-plugin' ),					
				'type' => 'image_select',
				'desc' => esc_html__( 'The layout of the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => array('title' => esc_html__( 'Default', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/1c.png'),
					'gp-left-sidebar' => array('title' => esc_html__( 'Left Sidebar', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-right-sidebar' => array('title' => esc_html__( 'Right Sidebar', 'socialize-plugin' ),  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
					'gp-both-sidebars' => array( 'title' => esc_html__( 'Both Sidebars', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/both-sidebars.png' ),
					'gp-no-sidebar' => array('title' => esc_html__( 'No Sidebar', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/no-sidebar.png'),
					'gp-fullwidth' => array('title' => esc_html__( 'Fullwidth', 'socialize-plugin' ), 'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
				),	
				'default' => 'default',
			),
			
			array(
				'id'      => 'portfolio_item_left_sidebar',
				'type'    => 'select',
				'required' => array( 'portfolio_item_layout', '=', array( 'gp-left-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Left Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),

			array(
				'id'      => 'portfolio_item_right_sidebar',
				'type'    => 'select',
				'required' => array( 'portfolio_item_layout', '=', array( 'gp-right-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Right Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),
					 
		),
	);
    $metaboxes[] = array(
        'id' => 'portfolio-item-options',
        'title' => esc_html__( 'Portfolio Item Options', 'socialize-plugin' ),
        'post_types' => array( 'gp_portfolio_item' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $portfolio_item_options,
    );
    
    
	/*--------------------------------------------------------------
	bbPress Options
	--------------------------------------------------------------*/	

	$bbpress_options = array();
    $bbpress_options[] = array(
		'title' => esc_html__( 'General', 'socialize-plugin' ),		
		'desc' => esc_html__( 'By default most of these options are set from the Theme Options page to change all pages at once, but you can overwrite these options here so this page has different settings.', 'socialize-plugin' ),
		'icon' => 'el-icon-cogs',
		'fields' => array(

			array( 
				'id' => 'bbpress_page_header',
				'title' => esc_html__( 'Page Header', 'socialize-plugin' ),
				'type' => 'select',
				'desc' => esc_html__( 'The page header on the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'gp-standard-page-header' => esc_html__( 'Standard', 'socialize-plugin' ),
					'gp-large-page-header' => esc_html__( 'Large', 'socialize-plugin' ),
					'gp-fullwidth-page-header' => esc_html__( 'Fullwidth', 'socialize-plugin' ),
					'gp-full-page-page-header' => esc_html__( 'Full Page', 'socialize-plugin' ),
				),
				'default' => 'default',
			),
				
			array(
				'id' => 'bbpress_page_header_bg', 
				'title' => esc_html__( 'Page Header Image Background', 'socialize-plugin' ),
				'type'      => 'media',		
				'mode'      => false,	
				'required' => array( 'bbpress_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The background of the page header.', 'socialize-plugin' ),
				'default' => '',
			),	

			array(
				'id' => 'bbpress_page_header_text', 
				'title' => esc_html__( 'Page Header Text', 'socialize-plugin' ),
				'type'      => 'text',	
				'required' => array( 'bbpress_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The text in the page header.', 'socialize-plugin' ),
				'default' => '',
			),
		
			array( 
				'id' => 'bbpress_layout',
				'title' => esc_html__( 'Page Layout', 'socialize-plugin' ),					
				'type' => 'image_select',
				'desc' => esc_html__( 'The layout of the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => array('title' => esc_html__( 'Default', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-left-sidebar' => array('title' => esc_html__( 'Left Sidebar', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-right-sidebar' => array('title' => esc_html__( 'Right Sidebar', 'socialize-plugin' ),  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
					'gp-both-sidebars' => array( 'title' => esc_html__( 'Both Sidebars', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/both-sidebars.png' ),
					'gp-no-sidebar' => array('title' => esc_html__( 'No Sidebar', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/no-sidebar.png'),
					'gp-fullwidth' => array('title' => esc_html__( 'Fullwidth', 'socialize-plugin' ), 'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
				),	
				'default' => 'default',
			),
			
			array(
				'id'      => 'bbpress_left_sidebar',
				'type'    => 'select',
				'required' => array( 'bbpress_layout', '=', array( 'gp-left-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Left Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),

			array(
				'id'      => 'bbpress_right_sidebar',
				'type'    => 'select',
				'required' => array( 'bbpress_layout', '=', array( 'gp-right-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Right Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),			

		),
	);
    $metaboxes[] = array(
        'id' => 'bbpress-options',
        'title' => esc_html__( 'bbPress Options', 'socialize-plugin' ),
        'post_types' => array( 'forum', 'topic' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $bbpress_options,
    );
    
	
	/*--------------------------------------------------------------
	Events Post Options
	--------------------------------------------------------------*/	

	$events_post_options = array();
    $events_post_options[] = array(
		'title' => esc_html__( 'General', 'socialize-plugin' ),		
		'desc' => esc_html__( 'By default most of these options are set from the Theme Options page to change all pages at once, but you can overwrite these options here so this page has different settings.', 'socialize-plugin' ),
		'icon' => 'el-icon-cogs',
		'fields' => array(

			array( 
				'id' => 'events_post_page_header',
				'title' => esc_html__( 'Page Header', 'socialize-plugin' ),
				'type' => 'select',
				'desc' => esc_html__( 'The page header on the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => esc_html__( 'Default', 'socialize-plugin' ),
					'gp-standard-page-header' => esc_html__( 'Standard', 'socialize-plugin' ),
					'gp-large-page-header' => esc_html__( 'Large', 'socialize-plugin' ),
					'gp-fullwidth-page-header' => esc_html__( 'Fullwidth', 'socialize-plugin' ),
					'gp-full-page-page-header' => esc_html__( 'Full Page', 'socialize-plugin' ),
				),
				'default' => 'default',
			),
				
			array(
				'id' => 'events_post_page_header_bg', 
				'title' => esc_html__( 'Page Header Image Background', 'socialize-plugin' ),
				'type'      => 'media',		
				'mode'      => false,	
				'required' => array( 'events_post_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The background of the page header.', 'socialize-plugin' ),
				'default' => '',
			),	
			
			array(
				'id' => 'events_post_page_header_text', 
				'title' => esc_html__( 'Page Header Text', 'socialize-plugin' ),
				'type'      => 'text',	
				'required' => array( 'events_post_page_header', '!=', 'gp-standard-page-header' ),
				'desc' => esc_html__( 'The text in the page header.', 'socialize-plugin' ),
				'default' => '',
			),
			
			array( 
				'id' => 'events_post_layout',
				'title' => esc_html__( 'Page Layout', 'socialize-plugin' ),					
				'type' => 'image_select',
				'desc' => esc_html__( 'The layout of the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => array('title' => esc_html__( 'Default', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-left-sidebar' => array('title' => esc_html__( 'Left Sidebar', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-right-sidebar' => array('title' => esc_html__( 'Right Sidebar', 'socialize-plugin' ),  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
					'gp-both-sidebars' => array( 'title' => esc_html__( 'Both Sidebars', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/both-sidebars.png' ),
					'gp-no-sidebar' => array('title' => esc_html__( 'No Sidebar', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/no-sidebar.png'),
					'gp-fullwidth' => array('title' => esc_html__( 'Fullwidth', 'socialize-plugin' ), 'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
				),	
				'default' => 'default',
			),
			
			array(
				'id'      => 'events_post_left_sidebar',
				'type'    => 'select',
				'required' => array( 'events_post_layout', '=', array( 'gp-left-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Left Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),

			array(
				'id'      => 'events_post_right_sidebar',
				'type'    => 'select',
				'required' => array( 'events_post_layout', '=', array( 'gp-right-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Right Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),		

		),
	);
    $metaboxes[] = array(
        'id' => 'events-post_options',
        'title' => esc_html__( 'Events Post Options', 'socialize-plugin' ),
        'post_types' => array( 'tribe_events' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $events_post_options,
    );
    
    
	/*--------------------------------------------------------------
	Product Options
	--------------------------------------------------------------*/	

    $product_options = array();
    $product_options[] = array(
		'desc' => esc_html__( 'By default most of these options are set from the Theme Options page to change all pages at once, but you can overwrite these options here so this page has different settings.', 'socialize-plugin' ),
        'fields' => array( 
		
			array( 
				'id' => 'product_layout',
				'title' => esc_html__( 'Product Page Layout', 'socialize-plugin' ),					
				'type' => 'image_select',
				'desc' => esc_html__( 'The layout of the page.', 'socialize-plugin' ),
				'options' => array(
					'default' => array('title' => esc_html__( 'Default', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/1c.png'),
					'gp-left-sidebar' => array('title' => esc_html__( 'Left Sidebar', 'socialize-plugin' ),   'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
					'gp-right-sidebar' => array('title' => esc_html__( 'Right Sidebar', 'socialize-plugin' ),  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
					'gp-both-sidebars' => array( 'title' => esc_html__( 'Both Sidebars', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/both-sidebars.png' ),
					'gp-no-sidebar' => array('title' => esc_html__( 'No Sidebar', 'socialize-plugin' ), 'img' => get_template_directory_uri() . '/lib/images/no-sidebar.png'),
					'gp-fullwidth' => array('title' => esc_html__( 'Fullwidth', 'socialize-plugin' ), 'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
				),	
				'default' => 'default',
			),
			
			array(
				'id'      => 'product_left_sidebar',
				'type'    => 'select',
				'required' => array( 'product_layout', '=', array( 'gp-left-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Left Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),

			array(
				'id'      => 'product_right_sidebar',
				'type'    => 'select',
				'required' => array( 'product_layout', '=', array( 'gp-right-sidebar', 'gp-both-sidebars' ) ),
				'title'   => esc_html__( 'Right Sidebar', 'socialize-plugin' ),
				'desc' => esc_html__( 'The sidebar to display.', 'socialize-plugin' ),
				'data'    => 'sidebar',
				'default' => '',
			),
					 
		),
	);
    $metaboxes[] = array(
        'id' => 'product-options',
        'title' => esc_html__( 'Product Options', 'socialize-plugin' ),
        'post_types' => array( 'product' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $product_options,
    );
    
    
	/*--------------------------------------------------------------
	Slide Options
	--------------------------------------------------------------*/	

    $slide_options = array();
    $slide_options[] = array(
        'fields' => array( 

			array(
				'id'       => 'slide_caption_title',
				'type'     => 'text',
				'title'    => esc_html__( 'Caption Title', 'socialize-plugin' ),
				'desc' => esc_html__( 'The caption title for the slide.', 'socialize-plugin' ),
				'default' => '',
			),
			
			array(
				'id'       => 'slide_caption_text',
				'type'     => 'textarea',
				'title'    => esc_html__( 'Caption Text', 'socialize-plugin' ),
				'desc' => esc_html__( 'The caption text for the slide.', 'socialize-plugin' ),
				'default' => '',
			),	
					
			array(
				'id'       => 'slide_link',
				'type'     => 'text',
				'title'    => esc_html__( 'Link', 'socialize-plugin' ),
				'desc'     => esc_html__( 'The link which your post goes to.', 'socialize-plugin' ),
				'validate' => 'url',
				'default' => '',
			),
			
			array( 
				'id' => 'slide_link_target',
				'title' => esc_html__( 'Link Target', 'socialize-plugin' ),
				'type' => 'button_set',
				'desc' => esc_html__( 'The target for the link.', 'socialize-plugin' ),
				'options' => array(
					'_self' => esc_html__( 'Same Window', 'socialize-plugin' ),
					'_blank' => esc_html__( 'New Window', 'socialize-plugin' ),
				),
				'default' => '_self',
			),			
					 
		),		
	);
    $metaboxes[] = array(
        'id' => 'slide-options',
        'title' => esc_html__( 'Slide Options', 'socialize-plugin' ),
        'post_types' => array( 'gp_slide' ),
        'position' => 'normal',
        'priority' => 'high',
        'sections' => $slide_options,
    );
        
    // Kind of overkill, but ahh well.  ;)
    $metaboxes = apply_filters( 'gp_redux_metabox_options', $metaboxes );

    return $metaboxes;
  }
  add_action('redux/metaboxes/'.$redux_opt_name.'/boxes', 'ghostpool_add_metaboxes');
endif;

// The loader will load all of the extensions automatically based on your $redux_opt_name
require_once(dirname(__FILE__).'/loader.php');